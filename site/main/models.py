from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserData(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    is_ta = models.BooleanField()
    is_teacher = models.BooleanField()

    def __unicode__(self):
        return self.user.username

class Course(models.Model):
    id = models.AutoField(primary_key = True)
    teacher = models.ForeignKey(User)
    name = models.CharField(max_length = 30)
    is_active = models.BooleanField()
    description = models.CharField(max_length = 1000, blank = True)
    syllabus = models.CharField(max_length = 10000, blank = True)
    grading = models.CharField(max_length = 10000, blank = True)
    prereq = models.CharField(max_length = 10000, blank = True)


    def __unicode__(self):
        ret = self.name + " - " + self.teacher.username
        if not self.is_active:
            ret = "[inactive] " + ret

        return ret;

class Registration(models.Model):
    user = models.ForeignKey(User)
    course = models.ForeignKey(Course)
    time = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        ret = self.user.username + " registered for " + self.course.name

class TA(models.Model):
    user = models.ForeignKey(User)
    course = models.ForeignKey(Course)
    time = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        ret = self.user.username + " is TA for " + self.course.name

class Announcement(models.Model):
    id = models.AutoField(primary_key = True)
    author = models.ForeignKey(User)
    time = models.DateTimeField(auto_now_add = True)
    message = models.CharField(max_length = 500)

    def __unicode__(self):
        ret = author + "[" + time + "]: " + message
